Version 0.2.0
- Added new separate page for creating reviews.
- Database now in Parse
- Added stars(ratings) to reviews

Version 1.0.0
- Added login and registration pages
- Added route protection and user authentication
- Modified navbar to have auth and non-auth versions
- Added logout button to auth-navbar
- Redirects now go to login page if path being accessed is protected & user is unauthenticated
    - Non existing paths route to home

Version 1.1.0
- Added comments on reviews
    - only authenticated users can comment

Version 1.2.0
- Ratings are visble on reviews as stars 
 - custom images
- basic submission type (review & comment) functionality completed 

Version 1.3.0 
- Users can now add a category to their reviews
- A new filter is located above review lists to filter by category

Version 1.4.0
- Added tool to delete account
    - Removes all reviews made by author