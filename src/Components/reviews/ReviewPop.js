/** This is a child module of the reviewlist. Formats the single review item. */
import { useState } from "react";
import CommentList from "../comment/CommentList"
import CommentBtn from "../comment/CommentButton"
import star from "../../images/star.png"
import star1 from "../../images/star1.png"
import star2 from "../../images/star2.png"
import star3 from "../../images/star3.png"
import star4 from "../../images/star4.png"
import star5 from "../../images/star5.png"

const ReviewPop = ({review,refresh,setRefresh}) => {
  const rating = review.get("stars");

  function stars (rating){
    console.log("ReviewPop stars")
      switch (rating) {
        case 1:
          return star1;
        case 2:
          return star2;
        case 3:
          return star3;
        case 4:
          return star4;
        case 5:
          return star5;
        default:
          return star;
    }
  }

    return (
        <div key={review.id} id="reviewPop" >
          <div id="reviewText">
            <div id="revHead">
            <h3>{review.get("title")}</h3>
            <img src={stars(rating)} id="star" alt="Rating"/>
            </div>
            <hr/>
            <i>Review by: {review.get("fname")} {review.get("lname")}</i>
            <br />
            <i>{review.get("updatedAt").toString()}</i> | <i>{review.get("meal")}</i>
          </div>
            <div id="reviewText">
            {review.get("text")}
            </div>
            <hr />
              < CommentBtn  review={review} setRefresh={setRefresh}/>
            <div id="reviewText">
              <CommentList review={review} refresh={refresh} setRefresh={setRefresh}/>
            </div>
        </div>
    );
};

export default ReviewPop;
