/** This is a child module of the reviewlist. */
const ReviewPop = (review) => {

    return (
          <div id="comments">
            <h3>{review.get("title")}</h3>
        </div>
    );
};

export default ReviewPop;
