/** This is a child module of the reviewlist. Formats the single review item. */
import star from "../../images/star.png"
import star1 from "../../images/star1.png"
import star2 from "../../images/star2.png"
import star3 from "../../images/star3.png"
import star4 from "../../images/star4.png"
import star5 from "../../images/star5.png"

const ReviewItem = (review,onClick) => {
  const rating = review.get("stars");

  function stars (rating){
    console.log("ReviewItem stars")
      switch (rating) {
        case 1:
          return star1;
        case 2:
          return star2;
        case 3:
          return star3;
        case 4:
          return star4;
        case 5:
          return star5;
        default:
          return star;
    }
  }


    return (
        <li onClick={() => onClick(review.id)} key={review.id} >
          <div id="titleBox">
            <h3>{review.get("title")}</h3>
            <img src={stars(rating)} id="star" alt="Rating"/>
          </div>
          <div id="reviewBox">
            <i>Review by: {review.get("fname")} {review.get("lname")}</i>
            <br />
            <i>{review.get("updatedAt").toDateString()}</i> | <i>{review.get("meal")}</i>
          </div>
        </li>
    );
};

export default ReviewItem;
