/** Returns the list of reviews. Also a child module of the main container. */
import { useEffect, useState } from "react";
import ReviewPop from "./ReviewPop.js";
import ReviewItem from "./ReviewItem.js";
import { getReviews, getbyId } from "../../Services/ReviewService.js";
import "../../css/review/review.css"
import "../../css/review/reviewPop.css"

const categories = ["sports", "food", "places", "entertainment", "music", "arts"]

const ReviewsListModule = () => {
    const [allReviews, setReviews] = useState([]);
    const [filteredReviews, setFilteredReviews] = useState([]);
    const [click, setClick] = useState(false);
    const [getpop, setGetPop] = useState(false);
    const [revId, setrevId] = useState(null);
    const [popRev, setpopRev] = useState(null);
    const [refresh, setRefresh] = useState(false);
    const [filterDict, setFilter] = useState({});

    useEffect(() => {
        getReviews().then((allReviews) => {
            setReviews(allReviews);
            setFilteredReviews(allReviews);
        });
    }, []);

    useEffect(() => {
        if (click) {
            getbyId(revId).then((review) => {
                console.log("clicked on review", review);
                setpopRev(review);
                setGetPop(true);
                setClick(false);
            });
        }
    }, [click]);

    const reviewClick = (id) => {
        setrevId(id);
        setClick(true);
    }

    const filterClick = (category) => {
        let filters = filterDict;
        if (filterDict[category]) {
            delete filters[category];
            setFilter(filters);
        }
        else {
            filters[category] = category;
            setFilter(filters);
        }
        let filteredReviewsTemp = []
        allReviews.forEach(review => {
            if (filterDict[review.get("category")]) {
                filteredReviewsTemp.push(review)
            }
        })
        if (Object.keys(filterDict).length > 0)
            setFilteredReviews(filteredReviewsTemp)
        else
            setFilteredReviews(allReviews)
        setRefresh(true)
    }

    const CategoryBubble = (category) => {
        return (
            !filterDict[category] ?
                <button key={category} onClick={() => filterClick(category)} style={{ margin: '5px', borderRadius: '5px', borderStyle: 'inset', backgroundColor: '#f9f9f9', padding: '2px', alignSelf: 'center' }}>
                    {category}
                </button>
                :
                <button key={category} onClick={() => filterClick(category)} style={{ margin: '5px', borderRadius: '5px', borderStyle: 'inset', backgroundColor: 'grey', padding: '2px', alignSelf: 'center' }}>
                    {category}
                </button>
        )
    }

    return (
        <div>
            <div>
                <ul style={{ display: 'flex', overflow: 'scroll', borderStyle: 'inset', backgroundColor: '#cdcdcd' }}>
                    <h3>Filter By:</h3>
                    {categories.map((category) => CategoryBubble(category))}
                </ul>
            </div>
            <div id="grid">
                <div className="reviewlist" id="list">
                    <ul>
                        {filteredReviews.map((review) => ReviewItem(review, reviewClick))}
                    </ul>
                </div>
                <div className="reviewPop" id="pop">
                    {getpop ? (
                        <ReviewPop review={popRev} refresh={refresh} setRefresh={setRefresh} />
                    ) : (
                        null
                    )}
                </div>
            </div>
        </div>
    );
};

export default ReviewsListModule;
