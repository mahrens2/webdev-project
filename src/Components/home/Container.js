import ReviewsListModule from "../reviews/ReviewsList.js";

/** This is the "parent" module of the whole page */
const MainModule = () => {
  return (
    <div className="topHome">
        <ReviewsListModule />
    </div>
  );
};

export default MainModule;
