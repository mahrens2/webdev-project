/** Returns the list of reviews. Also a child module of the main container. */
import { useEffect, useState } from "react";
import ReviewPop from "../reviews/ReviewPop.js";
import ReviewItem from "../reviews/ReviewItem.js";
import DelReviewMod from "./deleteReview"
import { getUserReviews, getbyId } from "../../Services/ReviewService.js";
import { deleteUser } from "../../Services/AuthService.js"
import "../../css/review/review.css"
import "../../css/review/reviewPop.css"
import Parse from "parse";
import { useHistory } from "react-router";

const ReviewListUser = () => {

    const categories = ["sports", "food", "places", "entertainment", "music", "arts"]

    const [allReviews, setReviews] = useState([]);
    const [click, setClick] = useState(false);
    const [getpop, setGetPop] = useState(false);
    const [revId, setrevId] = useState(null);
    const [popRev, setpopRev] = useState(null);
    const [refresh, setRefresh] = useState(false);
    const [filterDict, setFilter] = useState({});
    const [filteredReviews, setFilteredReviews] = useState([]);

    const history = useHistory();
    const user = Parse.User.current()
    var name = ''
    if(user){
      name = user.get("firstName")
    }
    useEffect(() => {
        getUserReviews().then((allReviews) => {
            console.log("usrclick");
            setReviews(allReviews);
            setFilteredReviews(allReviews);
        });
    }, []);

    useEffect(() => {
        if (click) {
            getbyId(revId).then((review) => {
                setpopRev(review);
                setGetPop(true);
                setClick(false);
            });
        }
    }, [click]);

    const onClick = (id) => {
        setrevId(id);
        setClick(true);
    }

    const filterClick = (category) => {
        let filters = filterDict;
        if (filterDict[category]) {
            delete filters[category];
            setFilter(filters);
        }
        else {
            filters[category] = category;
            setFilter(filters);
        }
        let filteredReviewsTemp = []
        allReviews.forEach(review => {
            if (filterDict[review.get("category")]) {
                filteredReviewsTemp.push(review)
            }
        })
        if (Object.keys(filterDict).length > 0)
            setFilteredReviews(filteredReviewsTemp)
        else
            setFilteredReviews(allReviews)
    }

    const CategoryBubble = (category) => {
        return (
            !filterDict[category] ?
                <button key={category} onClick={() => filterClick(category)} style={{ margin: '5px', borderRadius: '5px', borderStyle: 'inset', backgroundColor: '#f9f9f9', padding: '2px', alignSelf: 'center' }}>
                    {category}
                </button>
                :
                <button key={category} onClick={() => filterClick(category)} style={{ margin: '5px', borderRadius: '5px', borderStyle: 'inset', backgroundColor: 'grey', padding: '2px', alignSelf: 'center' }}>
                    {category}
                </button>
        )
    }

    const UserPageHeader = () => {
        return (
            <div style={{display:'flex', justifyItems:"flex-end"}}>
                <h3 style={{margin:"10px"}}>Welcome, {name}</h3>
                <button
                    onClick={() => {
                        deleteUser(Parse.User.current());
                        history.goBack();
                    }}
                    style={{backgroundColor:'red'}}
                >
                    DELETE ACCOUNT
                </button>
            </div>
        )
    }

    return (
        <div>
            <UserPageHeader />
            <div>
                <ul style={{ display: 'flex', overflow: 'scroll', borderStyle: 'inset', backgroundColor: '#cdcdcd' }}>
                    <h3>Filter By:</h3>
                    {categories.map((category) => CategoryBubble(category))}
                </ul>
            </div>
            <div id="grid">
                <div className="reviewlist" id="list">
                    <ul>
                        {filteredReviews.map((review) => ReviewItem(review, onClick))}
                    </ul>
                </div>
                <div className="reviewPop" id="pop">
                    {getpop ? (
                      <div>
                        <ReviewPop review={popRev} refresh={refresh} setRefresh={setRefresh} />
                        <DelReviewMod id={revId} filterClick={filterClick}/>
                      </div>
                    ) : (
                        null
                    )}
                </div>
            </div>
        </div>
    );
};

export default ReviewListUser;
