import React, { useEffect, useState } from "react";
import { removeUserReview } from "../../Services/ReviewService"

const DelReviewMod = ({id,filterClick}) => {
  const [click,setClick] = useState(false);

  useEffect(() => {
    if(click){
      removeUserReview(id).then(() => {
        setClick(false);
        filterClick("none")
      });
    }
  },[click]);

  const buttonHandler = () => {
    setClick(true);
  };
  return (
    <div style={{paddingLeft: 10}}>
      <button onClick={buttonHandler}>Delete this review</button>
    </div>
  );
};

export default DelReviewMod;
