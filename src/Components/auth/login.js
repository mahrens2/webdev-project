import React, { useEffect, useState } from "react";
import {Redirect} from "react-router-dom";
import { loginUser } from "../../Services/AuthService.js";
import AuthFormLogin from "./AuthFormLogin";
import Navigation from "../nav/navigation.js";

const AuthLogin = () => {
  const [userl, setloginUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: ""
  });
  const [add, setAdd] = useState(false);
  const [login, setLogin] = useState(false);
  // useEffect that run when changes are made to the state variable flags
  useEffect(() => {
    if (userl && add) {
      loginUser(userl).then((userLogin) => {
        if (userLogin) {
          alert(
            `${userLogin.get("firstName")}, you successfully logged in!`
          );
        }
        // TODO: redirect user to main app
        setLogin(true);
        setAdd(false);
      });
    }
  }, [userl, add]);

  const onChangeHandler = (e) => {
    e.preventDefault();
    const { name, value: newValue } = e.target;

    setloginUser({
      ...userl,
      [name]: newValue
    });
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();
    console.log("submitted: ", e.target);
    setAdd(true);
  };

  return (
    <div>
    {login ?  (<Redirect to="/home"/>) : (
      <AuthFormLogin
        user={userl}
        onChange={onChangeHandler}
        onSubmit={onSubmitHandler}
      />
    )}
    </div>
  );
};

export default AuthLogin;
