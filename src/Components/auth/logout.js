import React, { useEffect, useState } from "react";
import {logoutUser} from  "../../Services/AuthService.js"
import "../../css/nav.css"
import { useHistory } from "react-router";
const LogoutModule = ({setRef}) => {
  const [click,setClick] = useState(false);

  useEffect(() => {
    if(click){
      logoutUser().then(() => {
        setClick(false);
        setRef(true)
      });
    }
  },[click]);

  const buttonHandler = () => {
    setClick(true);
  };

  return (
      <button onClick={buttonHandler}>Logout</button>
  );
};

export default LogoutModule;
