import MainModule from "./home/Container.js";
import CreateReviewModule from "./form/CreateReview.js";
import NaviAuth from "./nav/naviAuth.js";
import AuthRegister from "./auth/register.js";
import AuthLogin from "./auth/login.js";
import ReviewListUser from "./user/user.js"
/* for page routing */
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import MainForm from "./form/MainForm.js";

/** Following a similar structure to the one discussed in class */
const Components = () => {
  return (
    <Router>
      < NaviAuth />
      <Switch>
        <Route path="/" exact component={MainModule} />
        <Route path="/authForm" component={MainForm} />
        <Route path="/register" component={AuthRegister} />
        <Route path="/login" component={AuthLogin} />
        <Route path="/user/:email/authForm" component={CreateReviewModule} />
        <Route path="/user/:email" component={ReviewListUser} />
        <Redirect to="/"/>
      </Switch>
    </Router>
  );
};

export default Components;
