import { createReview } from "../../Services/ReviewService.js";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import Select from 'react-select'
/** This contains the form that is used to create a review. Also a child module */

const CreateReviewModule = () => {

    const categories = [
        { value: 'sports', label: 'sports' },
        { value: 'food', label: 'food' },
        { value: 'places', label: 'places' },
        { value: 'entertainment', label: 'entertainment' },
        { value: 'music', label: 'music' },
        { value: 'arts', label: 'arts' },
    ]


    const [newReview, setReview] = useState({
        email: "",
        title: "",
        meal: "",
        category: "",
        stars: 0,
        text: ""
    })
    const [selectedOption, setSelectedOption] = useState(null);
    const history = useHistory();

    const onChangeHandler = (e) => {
        // Continuously updating name to be added on submit
        setReview({
            ...newReview,
            [e.target.id]: e.target.value,
        });
    };

    //send review to db
    const handleSubmit = (e) => {
        // Save category...
        console.log("category:", selectedOption.value)
        let tempReview = newReview;
        tempReview["category"] = selectedOption.value;
        setReview(tempReview);
        e.preventDefault();
        console.log("submitted: ", newReview);
        console.log("category:", selectedOption.value)
        createReview(newReview).then(() => {
            history.push("/home");
        });
        history.goBack();
    };

    return (
        <div>
            <h2>Add a new review</h2>
            <form>
                <label htmlFor="title">
                    <b>Title: </b>
                </label>
                <input onChange={onChangeHandler} id="title" type="text" />
                <br />
                <label id="ratings" htmlFor="stars">
                    <b>Rating:</b>
                </label>
                <select onChange={onChangeHandler} id="stars" type="number" >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <br />
                <br />
                <label htmlFor="meal">
                    <b>Topic: </b>
                </label>
                <input onChange={onChangeHandler} id="meal" type="text" />
                <br />
                <label htmlFor="text">
                    <b>Subject: </b>
                </label>
                <textarea onChange={onChangeHandler} value={newReview.text} id="text" cols="40" rows="5"></textarea>
                <br />
                <label id="category" htmlFor="category">
                    <b>Category:</b>
                </label>
                <Select options={categories}
                    onChange={setSelectedOption}
                    value={selectedOption}
                    isSearchable={false} />
                <br />
                <button type="submit" id="formbtn" onClick={handleSubmit}>Submit</button>
            </form>
        </div>
    );
};

export default CreateReviewModule;
