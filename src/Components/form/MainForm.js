import React, { useEffect, useState } from "react";
import ProtectedRoute from "../../Common/AppTools/ProtectedRoute";
import CreateReviewModule from "./CreateReview";
import Parse from "parse";

const MainForm = () => {


  var loggedIn = false; //Replaced by Auth check. Need to be logged in to create review
  var  fullpath = "/denied";
  const currentUser = Parse.User.current();
  console.log("minfofmcuru",currentUser);
  if(currentUser){
    loggedIn = true;
    fullpath = "/user/" + currentUser.getEmail() + "/authForm";
  }
  // In this case the flag is acquired through a check box but it could also be received from other methods
  // What is a Parse.User method that would help here?
  return (
    <div>
      <ProtectedRoute
        exact
        path={fullpath} //Replace with email of user
        loggedIn={loggedIn}
      />
    </div>
  );
};

export default MainForm;
