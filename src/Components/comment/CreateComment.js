import { createComment } from "../../Services/ReviewService.js";
import CommentList from "../comment/CommentList"
import { useEffect, useState } from "react";

const CreateComment = ({review,setClick,setRefresh}) => {

  const [newComment,setComment] = useState({text:""});
  const [submit,setSubmit] = useState(false);

  useEffect(() => {
    if(newComment && submit){
      createComment(newComment,review).then(() => {
        setSubmit(false);
        setClick(false);
        setRefresh(true);
      });
    }
  },[newComment,submit]);

  const onChangeHandler = (e) => {
     // Continuously updating name to be added on submit
     setComment({
       ...newComment,
       [e.target.id] : e.target.value
     });
  };

  //send review to db
  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmit(true);
  };

  return (
    <div className="cmtform">
      <form>
          <label htmlFor="text">
              <b>Comment: </b>
          </label>
          <textarea onChange={onChangeHandler} id="text" cols="40" rows="5"></textarea>
          <br />
          <button type="submit" onClick={handleSubmit}>Submit</button>
      </form>
    </div>
  );

};

export default CreateComment;
