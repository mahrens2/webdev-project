const CommentItem = (comment) => {
    return (
        <li id="cmtItem" key={comment.id}>
            <b>{comment.get("fname")} {comment.get("lname")}</b> |
            <i> {comment.get("createdAt").toString()}</i>
            <br />
            {comment.get("commentText")}
        </li>
    );
};

export default CommentItem;
