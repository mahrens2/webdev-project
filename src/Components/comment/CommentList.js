/** Returns the list of Comments. Also a child module of the main container. */
import { useEffect, useState } from "react";
import CommentItem from "./CommentItem.js";
import { getComments} from "../../Services/ReviewService.js";
import "../../css/review/comments.css"

const CommentList = ({review,refresh,setRefresh}) => {
    const [allComments, setComments] = useState([]);

      useEffect(() => {
        console.log("cmlist");
          getComments(review).then((allComments) => {
             setComments(allComments);
             setRefresh(false);
          });
      }, [refresh]);

    return (
        <ul id="cmtList">
          {allComments.map((comment) => CommentItem(comment))}
        </ul>
    );
};

export default CommentList;
