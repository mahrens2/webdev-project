import React, { useEffect, useState } from "react";
import Parse from "parse";
import CreateComment from "./CreateComment"
import "../../css/nav.css"
import { useHistory } from "react-router";

const CommentBtn = ({review,setRefresh}) => {

  const [click,setClick] = useState(false);
  const currentUser = Parse.User.current();

  const buttonHandler = () => {
    setClick(!click);
  };

  return (
    <div id="cmtdiv">
      { currentUser ? (
        <div>
        <i>Comments:</i>
        <button id="cmtbtn" onClick={buttonHandler}>Comment</button>
        {click ? (< CreateComment review={review} setClick={setClick} setRefresh={setRefresh}/>)
          :
        ( null )}
        </div>
      ) : (
        null
      )}
    </div>
  );
};

export default CommentBtn;
