import React, { useEffect, useState } from "react";
import { useLocation } from 'react-router-dom'
import ProtectedRoute from "../../Common/AppTools/ProtectedRoute";
import Navigation from "./navigation";
import Parse from "parse";

const NaviAuth = () => {


  var loggedIn = false;
  const currentUser = Parse.User.current();
  const location = useLocation();

  if(currentUser){
    loggedIn = true;
  }

  return (
    <div>
    <Navigation loggedIn={loggedIn}/>
    </div>
  );
};

export default NaviAuth;
