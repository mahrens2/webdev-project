// Navigation bar component using react Link
import Parse from "parse"
import { useState, useEffect } from "react"
import { Link } from "react-router-dom";
import LogoutModule from "../auth/logout.js"
import { useHistory } from "react-router";
import "../../css/nav.css"

const Navigation = (props) => {
  const history = useHistory();
  const [path,setPath] = useState("/home/");
  const [refresh,setRefresh] = useState(false)
  const currentUser = Parse.User.current();

  useEffect(() => {
    if(props.loggedIn && currentUser) {
      setPath("/user/" + currentUser.getEmail());
    }
  })

  useEffect(() => {
    history.push("/home");
    setRefresh(false)
    history.goBack();
  },[refresh]);

  console.log("navfp",path)
  return (
    <div>
    { props.loggedIn ? (
    <div className="navi">
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/authForm">Review</Link>
          </li>
          <li>
            <Link to={path}>User</Link>
          </li>
          <li style={{float:"right"}}>
            < LogoutModule setRef={setRefresh} />
          </li>
        </ul>
      </nav>
    </div>
  ) : (
    <div className="navi">
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/register">Register</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
        </ul>
      </nav>
    </div>
  )}
  </div>
  );
};

export default Navigation;
