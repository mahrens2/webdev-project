import Parse from "parse";

Parse.initialize('wYvUF3Y33856m8P2YrRu3EXz1hiYzV0u9tlm0eVJ', 'ugxn3NLIRVJX9MNqsTgHLk3ZZtod88IxZlMqDKWy');
Parse.serverURL = 'https://parseapi.back4app.com/';


// Function to get all reviews
export const getReviews = () => {
    console.log("get review list");
    const Review = Parse.Object.extend("Review");
    const query = new Parse.Query(Review);
    query.descending("createdAt");
    return query.find().then((results) => {
        // returns array of Review objects
        return results;
    });
};

export const getUserReviews = () => {
    console.log("get user review list");
    const currentUser = Parse.User.current();
    const Review = Parse.Object.extend("Review");
    const query = new Parse.Query(Review);
    query.equalTo("createdBy",currentUser);
    query.descending("createdAt");
    return query.find().then((results) => {
        // returns array of Review objects
        return results;
    });
};

export const removeUserReview = (id) => {
    console.log("remove user review");
    const Review = Parse.Object.extend("Review");
    const query = new Parse.Query(Review);
    return query.get(id).then((review) => {
            review.destroy();
    });
};

export const removeAllUserReviews = () => {
    console.log("remove user review list");
    const currentUser = Parse.User.current();
    const Review = Parse.Object.extend("Review");
    const query = new Parse.Query(Review);
    query.equalTo("createdBy",currentUser);
    return query.find().then((results) => {
        // returns array of Review objects
        results.forEach((review) => {
            review.destroy();
        })
    });
};

export const getComments = (review) => {
    const Comment = Parse.Object.extend("comment");
    const query = new Parse.Query(Comment);
    query.equalTo("review",review);
    query.descending("createdAt");
    return query.find().then((results) => {
        // returns array of Review objects
        console.log("rescmt ",results);
        return results;
    });
};

export const getbyId = (id) => {
    console.log("get review single");
    const Review = Parse.Object.extend("Review");
    const query = new Parse.Query(Review);
    return query.get(id).then((review) => {
        // returns one review by id
        return review;
    });
};

// Function used to create a review. Takes the elements in the DOM
export const createComment = (comment,review) => {
    console.log("revOp; ",review)
    const currentUser = Parse.User.current();
    const Comment = Parse.Object.extend("comment");
    const newComment = new Comment();
    newComment.set("createdBy",currentUser);
    newComment.set("commentText", comment.text);
    console.log(currentUser.get("firstName"))
    newComment.set("fname",currentUser.get("firstName"));
    newComment.set("lname", currentUser.get("lastName"));
    newComment.set("review",review);
    return newComment.save()
      .then((result) => {
        return result;
    })
    .catch((error) => {
      alert(`Error: ${error.message}`);
    });
};

// Function used to create a review. Takes the elements in the DOM
export const createReview = (submission) => {
    const currentUser = Parse.User.current();
    const Review = Parse.Object.extend("Review");
    const newReview = new Review();
    newReview.set("title",submission.title);
    newReview.set("text", submission.text);
    console.log(currentUser.get("firstName"))
    newReview.set("fname",currentUser.get("firstName"));
    newReview.set("lname", currentUser.get("lastName"));
    newReview.set("createdBy", currentUser);
    newReview.set("category", submission.category);
    newReview.set("meal", submission.meal);
    newReview.set("stars",parseInt(submission.stars));
    console.log(submission)
    console.log(newReview);
    return newReview.save()
      .then((result) => {
        return result;
    })
    .catch((error) => {
      alert(`Error: ${error.message}`);
    });
};
