import Parse from "parse";
import { removeAllUserReviews } from "./ReviewService";


Parse.initialize('wYvUF3Y33856m8P2YrRu3EXz1hiYzV0u9tlm0eVJ', 'ugxn3NLIRVJX9MNqsTgHLk3ZZtod88IxZlMqDKWy');
Parse.serverURL = 'https://parseapi.back4app.com/';

// used in auth register component
export const createUser = async (newUser) => {
    const user = new Parse.User();

    user.set("username", newUser.email);
    user.set("firstName", newUser.firstName);
    user.set("lastName", newUser.lastName);
    user.set("password", newUser.password);
    user.set("email", newUser.email);

    console.log("User: ", user);
    return user
        .signUp()
        .then((newUserSaved) => {
            return newUserSaved;
        })
        .catch((error) => {
            alert(`Error: ${error.message}`);
        });
};

export const loginUser = async (user) => {

    let username = user.email
    let password = user.password
    return Parse.User.logIn(username, password)
        .then((result) => {
            return result;
        })
        .catch((error) => {
            alert(`Error: ${error.message}`);
        });
};

export const deleteUser = async (user) => {
    removeAllUserReviews().then(async () => {
        try {
            const result = await Parse.User.destroyAll([user]);
            Parse.User.logOut();
            return result;
        } catch (error) {
            alert(`Error: ${error.message}`);
        }})
};

export const logoutUser = () => {
  return Parse.User.logOut().then(() => {
    const currUser = Parse.User.current();
    if(!currUser){
      alert(
        `successfully logged out`
      );}
  })
  .catch((error) => {
    alert(`Error: ${error.message}`);
  });
}
