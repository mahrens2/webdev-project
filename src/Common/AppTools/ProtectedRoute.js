import React from "react";
import { Redirect, useLocation } from "react-router-dom";

// Based of example from class
// Defining the protected route. In this case, we want to make sure that users that aren't logged in can't add reviews.
const ProtectedRoute = ({ component: Component, loggedIn, ...rest }) => {

    const location = useLocation();
    console.log("pathpr",rest.path);
    console.log("logged in",loggedIn);

    return (
        <div>
            {loggedIn ? (
              <div>
              {rest.path === "/register" || rest.path === "/login" ? (
                <Redirect to="/home" />
              ) : (
                <Redirect to={rest.path} />
              )}
              </div>
            ) : (
              <div>
                {rest.path === "/denied" ? (
                  <div>
                  {alert(`Unauthorized! You must log in to visit this page`)}
                  <Redirect to="/login" />
                  </div>
                ) : (
                  <Redirect to={rest.path} />
                )}
              </div>
            )}
        </div>
    );
};

export default ProtectedRoute;
