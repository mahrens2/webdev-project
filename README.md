# WebDev Project

Members:
- Rafael Mendizabal
- Matthew Ahrens

# Description
NDReviews is a Web App that allows students to share useful information about the state of places and things on campus

# Problem
There is useful information to be shared by students on campus life that helps plan out your day:
- How is food at the DH today?
- Is it worth it to go to the Library today?
- Is an event worth attending? 
